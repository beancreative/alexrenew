(function ($, Drupal) {

  Drupal.behaviors.alexrenew = {
    attach: function(context, settings) {
      // PLACE CUSTOM SCRIPTS HERE...
	  
	  
	  
  	//display the search form on click
	$(".header-search-button").click(function() {
      //alert("search THIS");
		$( ".block-search-form" ).slideToggle( "slow", function() {
			// Animation complete.
			//alert("search THIS");
		});
    });
	
	//set the initial value of the search field, and the replace with nothign when focused on
	$('#edit-search-block-form--2').attr('value', 'Add your search terms and press enter...');
	$('#edit-search-block-form--2').click(function() {
	  this.value="";
	});
	
	
	
	
	// SITEMAP POP UP PANEL
	// Get a reference to the container.
	var container = $( ".sitemap-block" );
	var sitemapButton = $( ".sitemap-button" );
	
	//slide down as soon as page loads. note: hiding it or setting interval to zero breaks scroll bar
	container.slideUp( 1 );

	// Bind the link to toggle the slide.
	$( ".sitemap-button" ).click(
		function( event ){
			// Prevent the default event.
			event.preventDefault();

			// Toggle the slide based on its current visibility.
			if (container.is( ":visible" )){

				// Hide - slide up.
				container.slideUp( 200 );

			} else {

				// Show - slide down.
				container.slideDown( 500 );
			}
		}
	);




	// BAR CHART ON MY ACCOUNT PAGE
	// FOR OPTIONS, SEE http://www.chartjs.org/docs/#getting-started-creating-a-chart
	var ctx = document.getElementById("usage-history").getContext("2d");
	window.myBar = new Chart(ctx).Bar(barChartData, {
		responsive : true,
		tooltipTemplate: "<%= value %>,000 Gallons",
		tooltipFillColor: "rgba(0,153,203,0.8)"
	});

	
	
	
	  
	  
    }
  };
  
	

})(jQuery, Drupal);
